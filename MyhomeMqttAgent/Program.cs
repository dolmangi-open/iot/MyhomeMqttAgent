﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;

namespace MyhomeMqttClientService
{
    static class Program
    {
 

        public static frmConfig frm_config = null;
        public static pipe_client pipe = null;
        public static UserInform frmInformation = null;
        public static Worker workerObject = null;
        public static Thread workerThread = null;
        public static string broker_addr = "";
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Show the system tray icon.					
            using (ProcessIcon pi = new ProcessIcon())
            {
                pi.Display();
                pipe = new pipe_client("MyhomeMqttClientService", OnPipeMessage);

                // Make sure the application runs!
                Application.Run();
            }
        }


        public static void SetBrokerAddress(string addr)
        {
            broker_addr = addr;
            MyMessage msg = new MyMessage((int)PipeClientMsg.MSG_BrokerAddrChanged, broker_addr);
            pipe.client.PushMessage(msg);
        }
        public static string GetBrokerAddress()
        {
            return broker_addr;
        }




        public static  void ShutdownCancel()
        {
            if (pipe != null)
                pipe.push_message(PipeClientMsg.MSG_ShutdownCancel);
        }
        public static void ShutdownNow()
        {
            if (pipe != null)
                pipe.push_message(PipeClientMsg.MSG_ShutdownNow);
        }
        static void OnPipeMessage(MyMessage msg)
        {
            Console.WriteLine("Server says: {0}", msg);
           
            bool show_info = false;
            switch((PipeServerMsg)msg.id)
            {
                case PipeServerMsg.MSG_Shutdown:
                    msg.txt = "컴퓨터가 " + msg.arg + "초 후에 꺼집니다.\r\n 아래 버튼으로 취소 할 수 있습니다.";
                    show_info = true;
                    break;
                case PipeServerMsg.MSG_Reboot:
                    msg.txt = "컴퓨터가 " + msg.arg + "초 후에 재시작 됩니다\r\n 아래 버튼으로 취소 할 수 있습니다.";
                    show_info = true;
                    break;
                case PipeServerMsg.MSG_ForceShutdown:
                    msg.txt = "컴퓨터가 " + msg.arg + "초 후에 꺼집니다\r\n취소 할 수 없습니다.";
                    show_info = true;
                    break;
                case PipeServerMsg.MSG_Broker_addr:
                    broker_addr = msg.txt;
                    show_info = false;
                    break;
                case PipeServerMsg.MSG_Broker_connected:
                    show_info = true;
                    broker_addr = msg.txt;
                    msg.txt = "Connected!";
                    break;
                case PipeServerMsg.MSG_Broker_NotConnected:
                    show_info = true;
                    msg.txt = "Not Connected!";
                    break;
                default:
                    show_info = false;
                    break;
            }

            if (show_info)
            {
                if (frmInformation == null)
                {
                    workerObject = new Worker(msg);
                    Thread workerThread = new Thread(workerObject.DoWork);

                    // Start the worker thread.
                    workerThread.Start();
                    Console.WriteLine("main thread: Starting worker thread...");
                }
                else
                {
                    if (frmInformation.mySetInfoFunc != null)
                        frmInformation.Invoke(frmInformation.mySetInfoFunc, msg);
                }
            }

        }
    }
    public class Worker
    {
        MyMessage msg;
        public Worker(MyMessage _msg)
        {
            Program.frmInformation = new UserInform();
            msg = _msg;
        }
        ~Worker()
        {
            Program.frmInformation = null;
        }
        // This method will be called when the thread is started.
        public void DoWork()
        {
            Console.WriteLine("worker thread: working...");
            Program.frmInformation.set_information(msg);


            switch ((PipeServerMsg)msg.id)
            {
                case PipeServerMsg.MSG_Broker_addr:                    
                    break;
                case PipeServerMsg.MSG_Broker_connected:
                    break;
                case PipeServerMsg.MSG_Broker_NotConnected:
                    break;
                default:
                    break;
            }
            Program.frmInformation.ShowDialog();
            Program.frmInformation = null;
            Console.WriteLine("worker thread: terminating gracefully.");
        }
        public void RequestStop()
        {
            if (Program.frmInformation != null)
                Program.frmInformation.Close();
        }    
    }
}
