﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyhomeMqttClientService
{
    public partial class frmConfig : Form
    {
        public string broker_addr;

        public frmConfig()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MyRegistry reg = new MyRegistry();
            textBrokerAddress.Text = broker_addr;

            AcceptButton = btnOK;
            CancelButton = btnCancel;

        }

        private void textBrokerAddress_TextChanged(object sender, EventArgs e)
        {
            broker_addr = textBrokerAddress.Text;             
        }

   

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}
