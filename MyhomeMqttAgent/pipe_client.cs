﻿//using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NamedPipeWrapper;


using System;
using System.Runtime.Remoting.Channels.Ipc;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting;

namespace MyhomeMqttClientService
{
 

    class pipe_client
    {

        public NamedPipeClient<MyMessage> client;
        public delegate void dgOnMessage(MyMessage message);
        dgOnMessage msg_handler;

        public pipe_client(string pipeName, dgOnMessage func)
        {

            client = new NamedPipeClient<MyMessage>(pipeName);
            client.ServerMessage += OnServerMessage;
            client.Error += OnError;
            client.Start();
            Console.WriteLine("pipe_client wait for connection");
            client.WaitForConnection(5000);
            Console.WriteLine("pipe_client started");
            msg_handler = func;
        }
        ~pipe_client()
        {
            client.Stop();
            Console.WriteLine("pipe_client stopped");
            client = null;
        }

        public bool push_message(PipeClientMsg id, int arg, string msg="")
        {
            MyMessage mymsg = new MyMessage((int)id, arg, msg);
            client.PushMessage(mymsg);
            return true;
        }
        public bool push_message(PipeClientMsg id, string msg="")
        {
            MyMessage mymsg = new MyMessage((int)id, msg);
            client.PushMessage(mymsg);
            return true;
        }

        private void OnServerMessage(NamedPipeConnection<MyMessage, MyMessage> connection, MyMessage message)
        {
            if (msg_handler != null)
                msg_handler(message);
        }

        private void OnError(Exception exception)
        {
            Console.Error.WriteLine("ERROR: {0}", exception);
        }
    }

       
}