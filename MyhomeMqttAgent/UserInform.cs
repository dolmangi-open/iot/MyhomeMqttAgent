﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyhomeMqttClientService
{
    public partial class UserInform : Form
    {
        public delegate void dgSetInformation(MyMessage msg);
        public dgSetInformation mySetInfoFunc;
        public UserInform()
        {
            InitializeComponent();
            mySetInfoFunc = new dgSetInformation(set_information);
        }

        private void UserInform_Load(object sender, EventArgs e)
        {
            this.TopMost = true;
            this.BringToFront(); 
        }
        public void set_information(MyMessage msg)
        {
            txtInformation.Text = msg.txt;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Program.ShutdownCancel();
            Close();
        }

        private void btnShutdown_Click(object sender, EventArgs e)
        {
            Program.ShutdownNow();
            Close();
        }
    }
}
