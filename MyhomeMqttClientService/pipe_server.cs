﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NamedPipeWrapper;
using System.Diagnostics;


//using System;
using System.Runtime.Remoting.Channels.Ipc;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting;
using System.IO.Pipes;
using System.Security.AccessControl;

namespace MyhomeMqttClientService
{

    [Serializable]
    public class MyMessage
    {
        public int id;
        public int arg;
        public string txt;
        public MyMessage(int _id, string _txt)
        {
            id = _id;
            txt = _txt;
        }

        public MyMessage(int _id,int _arg, string _txt)
        {
            id = _id;
            arg = _arg;
            txt = _txt;
        }
        public override string ToString()
        {
            return string.Format("\"{0}\" (message ID = {1}, counter={2})", txt, id, arg);
        }
    }
    

    class pipe_server
    {
        NamedPipeServer<MyMessage> server;

        public System.Diagnostics.EventLog eventLog1;
        public delegate void dgOnMessage(int conn_id, MyMessage message);
        dgOnMessage msg_handler;

        public delegate void dgOnConnectionHandler(int conn_id);
        dgOnConnectionHandler con_handler;

        public int connection_count=0;

        public pipe_server(string pipeName, dgOnMessage msg_func, dgOnConnectionHandler con_func=null )
        {
            eventLog1 = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists("MyhomeMqttClientService"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "MyhomeMqttClientService", "");
            }
            eventLog1.Source = "MyhomeMqttClientService";
            eventLog1.Log = "";
            msg_handler = msg_func;
            con_handler = con_func;


            System.IO.Pipes.PipeSecurity ps = new PipeSecurity();
            ps.AddAccessRule(new PipeAccessRule("Users", PipeAccessRights.FullControl, AccessControlType.Allow));
            //ps.AddAccessRule(new PipeAccessRule(myPipeServerIdentity, PipeAccessRights.FullControl, AccessControlType.Allow));
        
            //System.Security.Principal.SecurityIdentifier sid = new System.Security.Principal.SecurityIdentifier(System.Security.Principal.WellKnownSidType.WorldSid, null);
            server = new NamedPipeServer<MyMessage>(pipeName,ps );
            server.ClientConnected += OnClientConnected;
            server.ClientDisconnected += OnClientDisconnected;
            server.ClientMessage += OnClientMessage;
            server.Error += OnError;
            server.Start();
                        
            eventLog1.WriteEntry("pipe_server started", EventLogEntryType.Information, 0);
        }

         ~pipe_server()
        {
            eventLog1.WriteEntry("pipe_server stopped", EventLogEntryType.Information, 0);
            server.Stop();
        }

        public bool push_message(PipeServerMsg id,int arg, string msg="")
        {
            MyMessage mymsg=new MyMessage((int)id, arg, msg);
            server.PushMessage(mymsg);
            return true;
        }
        public bool push_message(PipeServerMsg id,   string msg="")
        {
            MyMessage mymsg = new MyMessage((int)id,  msg);
            server.PushMessage(mymsg);
            return true;
        }
        private void OnClientConnected(NamedPipeConnection<MyMessage, MyMessage> connection)
        {
            Console.WriteLine("Client {0} is now connected!", connection.Id);
            eventLog1.WriteEntry("pipe_server Client connected! id=" + connection.Id.ToString() , EventLogEntryType.Information, 0);
            //connection.PushMessage(new MyMessage(new Random().Next(), "Welcome "+ connection.Id));
            connection_count++;

            if (con_handler != null)
                con_handler(connection.Id);
        }

        private void OnClientDisconnected(NamedPipeConnection<MyMessage, MyMessage> connection)
        {
            eventLog1.WriteEntry("pipe_server Client disconnected!", EventLogEntryType.Information, 0);
            connection_count--;
        }

        private void OnClientMessage(NamedPipeConnection<MyMessage, MyMessage> connection, MyMessage message)
        {
            if (msg_handler != null)
                msg_handler(connection.Id, message);

        }

        private void OnError(Exception exception)
        {
            eventLog1.WriteEntry("pipe_server Exception!" + exception.ToString(), EventLogEntryType.Error, 0);
            eventLog1.WriteEntry("ERROR: "+ exception);
        }
    }
    
 
    public class MyHomeMqttCommand : MarshalByRefObject
    {
        private static int command = 0;
        public int Command
        {
            get
            {
                return command;
            }
            set 
            {
                command = value;
            }
        }
    }

    class ipc_server
    {
        IpcServerChannel serverChannel;
        public MyHomeMqttCommand cmd = null;
        public System.Diagnostics.EventLog eventLog1;

        public ipc_server()
        {
            eventLog1 = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists("MyhomeMqttClientService"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "MyhomeMqttClientService", "");
            }
            eventLog1.Source = "MyhomeMqttClientService";
            eventLog1.Log = "";

        }
        public bool start()
        {
            System.Collections.IDictionary properties = new System.Collections.Hashtable();
            properties["portName"] = "PowerMqttClientService";
            properties["exclusiveAddressUse"] = false;
            serverChannel = new IpcServerChannel(properties, null);

            // "ipc://remote/counter"중 "remote"이름의 IPC채널을 만듭니다.
            //serverChannel = new IpcServerChannel("PowerMqttClientService");

            ChannelServices.RegisterChannel(serverChannel,false);
            eventLog1.WriteEntry("Registered the channels.");

            // 채널에 공통의 클래스오브젝트 Counter클 등록합니다.
            RemotingConfiguration.RegisterWellKnownServiceType(typeof(MyHomeMqttCommand), "command", WellKnownObjectMode.Singleton);

            // 채널출력
            eventLog1.WriteEntry("Listening on " + serverChannel.GetChannelUri());

            cmd = new MyHomeMqttCommand();
            eventLog1.WriteEntry("This is call number " + cmd.Command);

            eventLog1.WriteEntry("ipc_server started", EventLogEntryType.Information, 0);
            return true;
        }


        public bool stop()
        {
            if (cmd != null)
            {

            }
            if (serverChannel != null)
            {
                ChannelServices.UnregisterChannel(serverChannel);
                eventLog1.WriteEntry("Unregistered the channels.");
            }
            return true;
        }
    }
    
}

