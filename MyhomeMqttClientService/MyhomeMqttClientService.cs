﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Runtime.InteropServices;
using uPLibrary.Networking.M2Mqtt;
using System.Management;

using System.Net;
using uPLibrary.Networking.M2Mqtt.Messages;
using System.Net.Sockets;
using NamedPipeWrapper;

namespace MyhomeMqttClientService
{


    public enum ServiceState
    {
        SERVICE_STOPPED = 0x00000001,
        SERVICE_START_PENDING = 0x00000002,
        SERVICE_STOP_PENDING = 0x00000003,
        SERVICE_RUNNING = 0x00000004,
        SERVICE_CONTINUE_PENDING = 0x00000005,
        SERVICE_PAUSE_PENDING = 0x00000006,
        SERVICE_PAUSED = 0x00000007,
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct ServiceStatus
    {
        public long dwServiceType;
        public ServiceState dwCurrentState;
        public long dwControlsAccepted;
        public long dwWin32ExitCode;
        public long dwServiceSpecificExitCode;
        public long dwCheckPoint;
        public long dwWaitHint;
    };


    public enum PipeServerMsg
    {
        MSG_None,
        MSG_Shutdown,
        MSG_Reboot,
        MSG_ForceShutdown,
        MSG_Broker_addr,
        MSG_HeartbeatIntervalSec,
        MSG_Broker_connected,
        MSG_Broker_NotConnected
    }
    public enum PipeClientMsg
    {
        MSG_ShutdownCancel,
        MSG_ShutdownNow,
        MSG_BrokerAddrChanged,
    }

    public partial class MyhomeMqttClientService : ServiceBase
    {

        public const string MYHOME_STAT_TOPIC_PREFIX = "stat/myhome/pc/";
        public const string MYHOME_CMD_TOPIC = "cmnd/myhome/pc";
        public const string MYHOME_POWERCMD_RESET = "rst";
        public const string MYHOME_POWERCMD_SHOWDOWN = "sht";
        public const string MYHOME_POWERCMD_FORCE_SHOWDOWN = "fsd";
 
        private int eventId=0;

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(IntPtr handle, ref ServiceStatus serviceStatus);

         
        private System.Timers.Timer timer;
        private System.Timers.Timer shutdown_timer;
        private bool shutdown_timer_started = false;
        private ServiceStatus serviceStatus;
        private MqttClient mqtt_client;
        private pipe_server pipe;
        private string mqtt_broker_addr;
        private int mqtt_heartbeat_int_sec;
        //private ipc_server ipc;
        private MyRegistry reg;
        private string computer_name;
        private PipeServerMsg shutdown_mode = PipeServerMsg.MSG_None;
        private int shutdown_counter=0;
        
        public MyhomeMqttClientService()
        {
            InitializeComponent();
            eventLog1 = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists("MyhomeMqttClientService"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "MyhomeMqttClientService", "");
            }
            eventLog1.Source = "MyhomeMqttClientService";
            eventLog1.Log = "";


            reg = new MyRegistry();
            if (reg != null)
            {
                try
                {
                    mqtt_broker_addr = reg.GetBrokerAddr();
                    mqtt_heartbeat_int_sec = reg.GetHeartbeatIntSec();
                }
                catch (Exception e)
                {
                    eventLog1.WriteEntry("Registry handling error:" + e.ToString(), EventLogEntryType.Error, 0);
                }
            }

            eventLog1.WriteEntry("mqtt broker addr=[" + mqtt_broker_addr + " heartbeat interval=" + mqtt_heartbeat_int_sec.ToString()+ "secs");

            computer_name = Dns.GetHostName();
            //computer_name = computer_name.ToLower();
        }

        protected override void OnStart(string[] args)
        {
            
            pipe = new pipe_server("MyhomeMqttClientService", OnPipeMessageFromClient, OnPipeConnection);
            client_mqtt_connect();

            timer = new System.Timers.Timer();
            if (mqtt_heartbeat_int_sec < 1)
                mqtt_heartbeat_int_sec = 10;
            timer.Interval = mqtt_heartbeat_int_sec * 1000; // seconds  
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnHeartbeatTimer);
            timer.Start();

            shutdown_timer = new System.Timers.Timer();
            shutdown_timer.Interval = 1000;
            shutdown_timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnShutdownTimer);

            

            serviceStatus = new ServiceStatus();
            //serviceStatus.dwCurrentState = ServiceState.SERVICE_START_PENDING;
            //serviceStatus.dwWaitHint = 100000;
            serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
            eventLog1.WriteEntry("In OnStart HB timer=" + timer.Interval.ToString());
        }

        bool client_mqtt_connect()
        {
            if (mqtt_client == null)
            {
                try
                {
                    mqtt_client = new MqttClient(mqtt_broker_addr);
                }
                catch (Exception e)
                {
                    eventLog1.WriteEntry("New MqttClient Error :" + e.ToString(), EventLogEntryType.Error, 0);
                }
            }

            if (mqtt_client != null)
            {
                try
                {
                    mqtt_client.Connect(Guid.NewGuid().ToString());
                }
                catch (uPLibrary.Networking.M2Mqtt.Exceptions.MqttConnectionException e)
                {
                    eventLog1.WriteEntry("Connect Error :" + e.ToString(), EventLogEntryType.Error, 0);
                    mqtt_client = null;
                    return false;
                }

                // register to message received 
                mqtt_client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
                mqtt_client.MqttMsgSubscribed += client_MqttMsgSubscribed;
                mqtt_client.MqttMsgUnsubscribed += client_MqttMsgUnsubscribed;
                mqtt_client.MqttMsgPublished += client_MqttMsgPublished;

                // subscribe to the topic .. with QoS 2 
                mqtt_client.Subscribe(new string[] { MYHOME_CMD_TOPIC }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });

                string topic = MYHOME_STAT_TOPIC_PREFIX + computer_name;
                mqtt_client.Publish(topic, Encoding.UTF8.GetBytes("ON"));

            }

            return true;
        }

        void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            // handle message received 
            if (e == null || e.Message==null)
            {
                eventLog1.WriteEntry("Argument is null", EventLogEntryType.Information, eventId++);
                return;
            }
            string msg = Encoding.UTF8.GetString(e.Message);
            string ip = getIP();
            string arg = "";
            eventLog1.WriteEntry("command received=" + msg + " id=", EventLogEntryType.Information, eventId++);
            eventLog1.WriteEntry("hostname=" + computer_name + " ip=" + ip);
            if (msg.Length >= computer_name.Length && msg.Substring(0, computer_name.Length) == computer_name)
                arg = msg.Substring(computer_name.Length + 1);
            else if (msg.Length >= ip.Length && msg.Substring(0, ip.Length) == ip)
                arg = msg.Substring(ip.Length + 1);
            else
                return;

            if (arg.Length>4)
            { 
                if (arg.Substring(0, 3) == MYHOME_POWERCMD_RESET)       //reset
                    shutdown_mode = PipeServerMsg.MSG_Reboot;
                else if (arg.Substring(0, 3) == MYHOME_POWERCMD_SHOWDOWN)  //shutdown
                    shutdown_mode = PipeServerMsg.MSG_Shutdown;
                else if (arg.Substring(0, 3) == MYHOME_POWERCMD_FORCE_SHOWDOWN)  //force shutdown
                    shutdown_mode = PipeServerMsg.MSG_ForceShutdown;
                else return;

                if (shutdown_mode>0)
                {
                    eventLog1.WriteEntry("Timer setting :" + arg.Substring(4));
                    shutdown_counter = Int32.Parse(arg.Substring(4));
                    if (!shutdown_timer_started)
                    {
                        shutdown_timer.Start();
                        shutdown_timer_started = true;
                    }
                }

                try
                {
                    mqtt_client.Publish(MYHOME_STAT_TOPIC_PREFIX, Encoding.UTF8.GetBytes("reply:" + computer_name+ ":"+ msg));
                }
                catch ( Exception ee)
                {
                    eventLog1.WriteEntry(ee.ToString());
                }
            }

        }

         void OnPipeMessageFromClient(int conn_id, MyMessage msg)
        {
            eventLog1.WriteEntry("pipe_server OnClientMessage!", EventLogEntryType.Information, 0);
            eventLog1.WriteEntry("Client {0}" + conn_id + " says:" + msg, EventLogEntryType.Information, 0);
            switch((PipeClientMsg)msg.id)
            {
                case PipeClientMsg.MSG_ShutdownCancel:
                    shutdown_mode = 0;
                    shutdown_timer.Stop();
                    shutdown_timer_started = false;
                    eventLog1.WriteEntry("Shudown Canceled!!", EventLogEntryType.Information, 0);
                    break;
                case PipeClientMsg.MSG_ShutdownNow:
                    shutdown_timer.Stop();
                    shutdown_timer_started = false;
                    eventLog1.WriteEntry("Shudown Now!!", EventLogEntryType.Information, 0);
                    Shutdown(shutdown_mode);
                    shutdown_mode = PipeServerMsg.MSG_None;
                    break;
                case PipeClientMsg.MSG_BrokerAddrChanged:
                    {
                        MyRegistry reg = new MyRegistry();
                        mqtt_broker_addr = msg.txt;
                        reg.SetBrokerAddr(msg.txt);
                        eventLog1.WriteEntry("Registry Set:" + msg.txt, EventLogEntryType.Information, 0);

                        if (mqtt_client != null)
                        {
                            if (mqtt_client.IsConnected)
                                mqtt_client.Disconnect();
                            mqtt_client = null;
                        }
                        client_mqtt_connect();

                        if (pipe != null)  //broadcasting
                        {
                            if (mqtt_client != null && mqtt_client.IsConnected)
                                pipe.push_message(PipeServerMsg.MSG_Broker_connected, mqtt_broker_addr);
                            else
                                pipe.push_message(PipeServerMsg.MSG_Broker_NotConnected, mqtt_broker_addr);
                        }
                    }

                    break;
            }

        }

        void client_MqttMsgUnsubscribed(object sender, MqttMsgUnsubscribedEventArgs e)
        {
            eventLog1.WriteEntry("client_MqttMsgUnsubscribed", EventLogEntryType.Information, eventId++);
        }
        void client_MqttMsgPublished(object sender, MqttMsgPublishedEventArgs e)
        {
            eventLog1.WriteEntry("client_MqttMsgPublished", EventLogEntryType.Information, eventId++);
        }
        void client_MqttMsgSubscribed(object sender, MqttMsgSubscribedEventArgs e)
        {
            eventLog1.WriteEntry("client_MqttMsgSubscribed", EventLogEntryType.Information, eventId++);
        }
        protected override void OnStop()
        {
            timer.Stop();
            timer = null;
            shutdown_timer.Stop();
            shutdown_timer = null;

            eventLog1.WriteEntry("In onStop.");
            if (mqtt_client != null && mqtt_client.IsConnected)
            {
                try
                {
                    string topic = MYHOME_STAT_TOPIC_PREFIX + computer_name;
                    mqtt_client.Publish(topic, Encoding.UTF8.GetBytes("OFF"));
                    mqtt_client.Disconnect();
                }
                catch (Exception e)
                {
                    eventLog1.WriteEntry(e.ToString(), EventLogEntryType.Error, 0);
                }
            }

            serviceStatus.dwCurrentState = ServiceState.SERVICE_STOPPED;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }

        public void OnShutdownTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            if (shutdown_counter-- > 0)
            {
                if (pipe != null)
                    pipe.push_message(shutdown_mode, shutdown_counter, "");
            }
            else
            {
                shutdown_timer.Stop();
                shutdown_timer_started = false;
                Shutdown(shutdown_mode);
                shutdown_mode = PipeServerMsg.MSG_None;
            }
        }

        public void OnHeartbeatTimer(object sender, System.Timers.ElapsedEventArgs args)
        {

            if (mqtt_client== null )
                if (!client_mqtt_connect())
                    return;

            try
            {
                string topic = MYHOME_STAT_TOPIC_PREFIX + computer_name;
                mqtt_client.Publish(topic, Encoding.UTF8.GetBytes("ON"));
            }
            catch ( Exception e)
            {
                eventLog1.WriteEntry(e.ToString());
            }
            //
            
        }
        private void OnPipeConnection(int con_id)
        {
            if (pipe != null)
            {
                pipe.push_message(PipeServerMsg.MSG_Broker_addr, mqtt_broker_addr);
                pipe.push_message(PipeServerMsg.MSG_HeartbeatIntervalSec, mqtt_heartbeat_int_sec );
            }
        }

        private void eventLog1_EntryWritten(object sender, EntryWrittenEventArgs e)
        {

        }

        private string getIP()
        {
            IPAddress[] localIPs = Dns.GetHostAddresses(computer_name);
            string server_ip = "";
            foreach (IPAddress a in localIPs)
            {
                if (a.AddressFamily == AddressFamily.InterNetwork)
                {
                    server_ip = a.ToString();
                    break;
                }
            }
            return server_ip;
        }

        private static void Shutdown(PipeServerMsg mode)
        {

            string param = "";
            switch (mode)
            {
                case PipeServerMsg.MSG_Reboot:
                    param = "-f -r -t 1";
                    break;
                case PipeServerMsg.MSG_Shutdown:
                    param= " - f - s - t 1";
                    break;
                case PipeServerMsg.MSG_ForceShutdown:
                    param = "-f -s -t 1";
                    break;
            }

            ProcessStartInfo proc = new ProcessStartInfo();
            proc.FileName = "cmd";
            proc.WindowStyle = ProcessWindowStyle.Hidden;
            proc.Arguments = "/C shutdown " + param;
            Process.Start(proc);
        }
    }
}
