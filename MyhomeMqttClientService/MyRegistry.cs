﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Diagnostics;

namespace MyhomeMqttClientService
{


    public class MyRegistry
    {
        const string key = "HKEY_LOCAL_MACHINE\\SOFTWARE\\MyhomeMqttClientService";
        
        public MyRegistry()
        {
            
        }

        public bool SetBrokerAddr(string svr)
        {
            Registry.SetValue(key, "broker_addr", svr);
            return true;
        }
        public  string GetBrokerAddr()
        {           
            return (string)Registry.GetValue(key, "broker_addr", "192.168.0.93"); ;
        }
        
        public int GetHeartbeatIntSec()
        {
            return (int)Registry.GetValue(key, "heartbeat_int_sec", 10) ;
        }
        public bool SetHeartbeatIntSec(int sec)
        {
            Registry.SetValue(key, "heartbeat_int_sec", sec);
            return true;
        }

        public void SetAutorun_System(string appname, string value,  bool enable)
        {
            RegistryKey rk = Registry.LocalMachine.OpenSubKey
                ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            if (enable)
                rk.SetValue(appname,   value );
            else
                rk.DeleteValue(appname, false);
        }

        public string GetAutorun_System(string appname, string value)
        {
            RegistryKey rk = Registry.LocalMachine.OpenSubKey
               ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", false);
            return (string)rk.GetValue(appname);


        }

    }
}
