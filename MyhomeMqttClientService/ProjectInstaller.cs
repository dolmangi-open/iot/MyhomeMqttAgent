﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace MyhomeMqttClientService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        const string agent_name = "MyhomeMqttAgent";

        public ProjectInstaller()
        {
            InitializeComponent();
        }


        protected override void OnCommitted(System.Collections.IDictionary savedState)
        {
            new ServiceController(serviceInstaller1.ServiceName).Start();
            string agent_path = System.IO.Path.GetDirectoryName(this.Context.Parameters["AssemblyPath"]) + @"\" + agent_name + @".exe";
            System.Diagnostics.Process.Start(agent_path);

            /*
            MyRegistry reg = new MyRegistry();
            reg.SetAutorun_System(agent_name, agent_path, true);
            Debug.Print("committed : " + agent_name + " : " +agent_path);
            */
        }

        protected override void OnAfterUninstall(System.Collections.IDictionary savedState)
        {
            /*
            MyRegistry reg = new MyRegistry();
            reg.SetAutorun_System(agent_name, "",false);
            Debug.Print("OnAfterUninstall : " + agent_name);
            */
        }
        protected override void OnAfterRollback(System.Collections.IDictionary savedState)
        {
            /*
            MyRegistry reg = new MyRegistry();
            reg.SetAutorun_System(agent_name, "", false);
            Debug.Print("OnAfterRollback : " + agent_name);
            */
        }
    }
}
